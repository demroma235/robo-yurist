# URLS
## Неавторизованный пользователь
### /user/login
Страница авторизации
## Авторизированный пользователь
### Пользователь
#### /user/
Список заявляений
#### /user/claim/add
Добавление заявления
#### /user/claim/{id}
Просмотр заявления
### Администратор
#### /administrator/employees
Просмотр всех сотрудников
#### /administrator/settings
Просмотр настроек
#### /administrator/classification
todo
### Менеджер
#### /manager/claims
Просмотр заявлений без класса
#### /manager/classification
Просмотр классификаций
#### /manager/claim/{id}
Просмотр заявления
### Judge
#### /judge/claims
Просмотр всех заявлений у судьи
#### /judge/claim/{id}
Просмотр заявления
