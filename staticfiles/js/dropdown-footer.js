
var action = 1;

$('#button-first').on("click", firstDropdown);

function firstDropdown() {
    if ( action == 1 ) {
        $('#cont-first').animate({height:'140px'}, 300);
        action = 2;
    } else {
        $('#cont-first').animate({height: '0'}, 300);
        action = 1;
    }
}

$('#button-second').on("click", secondDropdown);

function secondDropdown() {
    if ( action == 1 ) {
        $('#cont-second').animate({height:'200px'}, 300);
        action = 2;
    } else {
        $('#cont-second').animate({height: '0'}, 300);
        action = 1;
    }
}

$('#button-third').on("click", thirdDropdown);

function thirdDropdown() {
    if ( action == 1 ) {
        $('#cont-third').animate({height:'200px'}, 300);
        action = 2;
    } else {
        $('#cont-third').animate({height: '0'}, 300);
        action = 1;
    }
}