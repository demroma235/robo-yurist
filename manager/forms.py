from django.forms import ModelForm

from user.models import Claim, Employee, Role

class ClaimSettingForm(ModelForm):
    def __init__(self, *args,**kwargs):
        super(ClaimSettingForm, self).__init__( *args,**kwargs)
        self.fields['judge'].queryset = Employee.objects.filter(role=Role.objects.get(sys_name='ROLE_JUDGE'))

    class Meta:
        model = Claim
        fields = ['judge', 'classification']