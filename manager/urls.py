from django.conf.urls import url, include
from django.contrib import admin

from manager import views

urlpatterns = [
    url(r'^claims/$', views.claims, name='manager_claims'),
    url(r'^classifications/$', views.classifications, name='manager_classifications'),
    url(r'^change-class/(?P<judge_id>\d+)$', views.change_judge_class, name='change_judge_class'),
    url(r'^change-claim/(?P<claim_id>\d+)$', views.change_claim, name='change_claim'),
    url(r'^list/$', views.judge_list, name='judge_list'),
    url(r'^employees_characteristics_manager/$', views.employees_characteristics, name='employees_characteristics_manager'),
    url(r'^employee_characteristics_create_manager/$', views.employee_characteristics_create,
        name='employee_characteristics_create_manager'),
]