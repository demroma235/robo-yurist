from django.contrib.auth.decorators import login_required
from django.db.models import Q
import datetime
from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from administrator.forms import EmployeeCharacteristicsForm
from administrator.models import EmployeeCharacteristic
from administrator.tools import getJudgeLoad
from manager.forms import ClaimSettingForm
from user.models import Claim, Classification, Employee, Role
from user.tools import checkEmployee
from manager.tools import getPath, getChildClassifications
from django.views.decorators.csrf import csrf_exempt


@login_required(login_url='login_page')
def claims(request):
    if checkEmployee(request.user, Role.ROLE_MANAGER):
        if request.method == 'GET':
            claims = Claim.objects.filter(Q(classification=None) | Q(judge=None))
            return render(request, 'manager_claims.html',{'claims': claims})

    else:
        return JsonResponse({'error': 'permission denied'})


@login_required(login_url='login_page')
def classifications(request):
    if checkEmployee(request.user, Role.ROLE_MANAGER):
        if request.method == 'GET':
            classifications = getChildClassifications()
            result = []
            for classification in list(classifications):
                result.append({
                    'item': classification,
                    'path': getPath(classification),
                })
            return render(request, 'classifications_help.html',{'classifications': result})
    else:
        return JsonResponse({'error': 'permission denied'})


@login_required(login_url='login_page')
def claim_page(request, claim_id):
    if checkEmployee(request.user, Role.ROLE_MANAGER):
        if request.method == 'GET':
            claim = Claim.objects.filter(id=claim_id).values()
            if not list(claim):
                return JsonResponse({'error': 'Claim is not exist or claim is not your'})
            role = Role.objects.get(name='Судья')
            judges = Employee.objects.filter(role=role).values()
            return JsonResponse({'result': {'claims': list(claim), 'judges': list(judges)}})
    else:
        return JsonResponse({'error': 'permission denied'})


@csrf_exempt
@login_required(login_url='login_page')
def change_judge_class(request, judge_id):
    if checkEmployee(request.user, Role.ROLE_MANAGER):
        judge = Employee.objects.get(id=judge_id, role=Role.objects.get(sys_name='ROLE_JUDGE'))
        if request.method == "POST":
            classification_ids = request.POST.getlist("classification_ids", [])
            classifications = Classification.objects.filter(id__in=classification_ids)
            judge.classification = classifications
            judge.save()
            return JsonResponse({'status': 'success'})
        elif request.method == "GET":
            classifications = getChildClassifications()
            result = []
            for classification in list(classifications):
                result.append({
                    'item': classification,
                    'path': getPath(classification),
                    'selected': classification in list(judge.classification.all())
                })
            return render(request, 'change_judge_class.html', {'classifications': result, 'judge': judge})
    else:
        return JsonResponse({'error': 'permission denied'})

@csrf_exempt
@login_required(login_url='login_page')
def change_claim(request, claim_id):
    claim = Claim.objects.get(id=claim_id)
    if claim == None:
        return JsonResponse({'status': 'error'})
    if request.method == "POST":
        form = ClaimSettingForm(request.POST, instance=claim)
        if form.is_valid():
            form.save()
            return JsonResponse({'status': 'success'})
        else:
            return JsonResponse({'status': 'error', 'message': str(form.errors.as_ul())})
    elif request.method == "GET":
        form = ClaimSettingForm(instance=claim)
        return render(request, 'change_claim.html', {'form': form})


@login_required(login_url='login_page')
def judge_list(request):
    if checkEmployee(request.user, Role.ROLE_MANAGER):
        if request.method == 'GET':
            judges = Employee.objects.filter(role=Role.objects.get(sys_name='ROLE_JUDGE'))
            return render(request, 'judge_list.html', {'judges': judges})
    else:
        return JsonResponse({'error': 'permission denied'})

@login_required(login_url='login_page')
def employees_characteristics(request):
    if checkEmployee(request.user, Role.ROLE_ADMIN) or checkEmployee(request.user, Role.ROLE_MANAGER):
        if request.method == 'GET':
            employees_characteristics = EmployeeCharacteristic.objects.all()
            result = getJudgeLoad(employees_characteristics)
            return render(request, 'employee_characteristics.html', {'employees': result})
    else:
        return JsonResponse({'error': 'permission denied'})

def employee_characteristics_create(request):
    if checkEmployee(request.user, Role.ROLE_ADMIN):
        if request.method == "POST":
            employee = request.POST['employee']
            qualification = request.POST['qualification']
            employee_characteristics = EmployeeCharacteristic()
            employee = Employee.objects.get(id=employee)
            employee_characteristics.date_finish = datetime.datetime.strptime(request.POST['date_finish'],
                                                                              '%d.%m.%Y').strftime('%Y-%m-%d')
            employee_characteristics.date_start = datetime.datetime.strptime(request.POST['date_start'],
                                                                             '%d.%m.%Y').strftime('%Y-%m-%d')
            employee_characteristics.employee = employee
            employee_characteristics.qualification = qualification
            employee_form = EmployeeCharacteristicsForm(request.POST, instance=employee_characteristics)
            if employee_characteristics.date_finish < employee_characteristics.date_start:
                employee_form.add_error(
                    'date_start',
                    'Дата конца отпуска не может быть раньше начала'
                )

            if employee_form.is_valid():
                employee_form.save()
                return JsonResponse({'status': 'success'})
            else:
                return JsonResponse({'status': 'error', 'message': str(employee_form.errors.as_ul())})
        elif request.method == "GET":
            employee_form = EmployeeCharacteristicsForm()
            return render(request, 'employee_characteristics_create.html', {'employee_form': employee_form})
    else:
        return JsonResponse({'error': 'permission denied'})

