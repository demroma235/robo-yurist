from user.models import Classification
from django.db.models import Q

def getPath(classifation):
    result = ''
    while (classifation.parent != None):
        result = classifation.parent.name + '/' + result
        classifation = classifation.parent
    if (len(result)>1 and result[-1]=='/'):
        result = result[0:-1]
    return result

def getChildClassifications():
    parent_ids = Classification.objects.values_list('parent_id', flat=True).exclude(
        parent_id__isnull=True).distinct()
    classifications = Classification.objects.exclude(id__in=list(parent_ids)).filter(enable=True).all()
    return classifications