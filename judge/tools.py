import json, requests

URI = 'http://jurist.xn--c1adjbwsnkio.pw:9010'


def getFactsByClaim(name):
    url = URI + '/api/annotated/' + name
    resp = requests.get(url=url)
    data = json.loads(resp.text)
    return data


def getFactsClaimsId():
    url = URI + '/api/annotated'
    try:
        resp = requests.get(url=url)
        data = json.loads(resp.text)
    except:
        data = []
    return data


def getClaimCaseHref(name):
    return URI + '/cases/' + name
