from django.conf.urls import url, include
from django.contrib import admin

from judge import views

urlpatterns = [
    url(r'^claims/$', views.claims, name='judge_claims'),
    url(r'^claim-facts/(?P<claim_id>\d+)$', views.claim_facts, name='claim_facts'),
    url(r'^change_status/(?P<claim_id>\d+)$', views.change_claim_status, name='claim_change_status'),
    url(r'claims/archive$', views.list_archive_claims, name='judge_archive_claims'),
    url(r'^claim/(?P<claim_id>\d+)$', views.claim_page, name='claim_page'),
]