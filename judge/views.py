import os

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.shortcuts import render, redirect
from docx import Document

from judge.tools import getFactsByClaim, getFactsClaimsId, getClaimCaseHref
from django.conf import settings

# Create your views here.
from django.views.decorators.csrf import csrf_exempt

from user.forms import FirstStatusClaimHistoryForm, SecondStatusClaimHistoryForm, StatusClaimHistoryForm
from user.models import Claim, Employee, Profile, StatusClaim, StatusClaimHistory, Role
from user.tools import checkEmployee
from django.db.models import Q


@login_required(login_url='login_page')
def claims(request):
    if checkEmployee(request.user, Role.ROLE_JUDGE):
        if request.method == 'GET':
            try:
                u = Employee.objects.get(user=request.user)
            except ObjectDoesNotExist:
                return redirect(to='employees')
            claims = Claim.objects.filter(judge=u).filter(~Q(status_id=StatusClaim.STATUS_ARCHIVE))
            return render(request, 'claims.html', {'claims': claims})
    else:
        return JsonResponse({'error': 'permission denied'})


@login_required(login_url='login_page')
def list_archive_claims(request):
    if checkEmployee(request.user, Role.ROLE_JUDGE):
        if request.method == 'GET':
            try:
                u = Employee.objects.get(user_id=request.user)
            except ObjectDoesNotExist:
                return redirect(to='employees')
            claims = Claim.objects.filter(judge=u).filter(status_id=StatusClaim.STATUS_ARCHIVE)
            return render(request, 'claims.html', {'claims': claims})
    else:
        return JsonResponse({'error': 'permission denied'})


@login_required(login_url='login_page')
@csrf_exempt
def change_claim_status(request, claim_id):
    request_type = request.GET.get('request_type')
    claim = Claim.objects.get(id=claim_id, judge=Employee.objects.get(user_id=request.user))
    claim_history = StatusClaimHistory()
    claim_history.claim_id = claim.id
    if claim == None:
        return JsonResponse({'status': 'error'})
    if request.method == "POST":
        form = StatusClaimHistoryForm(request.POST, instance=claim_history)
        if form.is_valid():
            form.instance.old_status_id = claim.status_id
            claim.status_id = form.instance.new_status_id
            form.instance.claim_id = claim.id
            form.save()
            claim.save()
            return JsonResponse({'status': 'success'})
        else:
            return JsonResponse({'status': 'error', 'message': str(form.errors.as_ul())})
    elif request.method == "GET":
        if (request_type == '1'):
            form = FirstStatusClaimHistoryForm(instance=claim_history)
            string = 'заявлению'
        else:
            form = SecondStatusClaimHistoryForm(instance=claim_history)
            string = 'делу'
        form.claim = claim
        form.old_status = claim.status
        history = StatusClaimHistory.objects.filter(claim_id=claim.id).order_by('date')

        return render(request, 'change_status.html', {'form': form, 'claim': claim, 'history': history ,'string':string})


@login_required(login_url='login_page')
def claim_facts(request, claim_id):
    if checkEmployee(request.user, Role.ROLE_JUDGE):
        if request.method == 'GET':
            empl = Employee.objects.get(user_id=request.user)
            claim = Claim.objects.get(judge=empl, id=claim_id)
            try:
                data = getFactsByClaim(claim.claim_file.name)
            except:
                return render(request, 'connection_error.html')

            return render(request, 'judge_claim_page.html', {'text': data['text'], 'facts': data['annotations']})
    else:
        return JsonResponse({'error': 'permission denied'})


@login_required(login_url='login_page')
def claim_page(request, claim_id):
    # if checkEmployee(request.user, Role.ROLE_JUDGE):
        if request.method == 'GET':
            try:
                empl = Employee.objects.get(user_id=request.user)
                claim = Claim.objects.get(judge=empl, id=claim_id)
            except ObjectDoesNotExist:
                claim = Claim.objects.get(id=claim_id)
            claim_file_path = str(claim.claim_file.file)
            filename, file_extension = os.path.splitext(claim_file_path)
            filepath = os.path.join(settings.MEDIA_ROOT, claim_file_path)
            f = open(filepath, 'rb')
            content = ''
            if (claim.claim_file.name not in getFactsClaimsId()):
                is_mark = False
                if (file_extension == '.docx'):
                    doc = Document(f)
                    fullText = []
                    for para in doc.paragraphs:
                        fullText.append(para.text)
                    content = '\n'.join(fullText)
            else:
                is_mark = True
                content = getClaimCaseHref(claim.claim_file.name)
            claim_history = StatusClaimHistory()
            claim_history.claim_id = claim.id
            form = SecondStatusClaimHistoryForm(instance=claim_history)
            form.old_status = claim.status
            form.new_status = claim.status
            return render(request, 'claim_page.html', {
                'is_mark': is_mark,
                'content': content,
                'form': form,
                'claim': claim
            })

    # else:
    #     return JsonResponse({'error': 'permission denied'})
