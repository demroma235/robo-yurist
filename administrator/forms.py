from datetimewidget.widgets import DateWidget, DateTimeWidget
from django import forms
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.forms import ModelForm, Form

from administrator.models import EmployeeCharacteristic
from user.models import Classification, Employee, Setting


class ClassificationForm(ModelForm):
    class Meta:
        model = Classification
        fields = '__all__'
        labels = {
            'parent': 'Родитель',
            'name': 'Название',
            'enable': 'Активировать',
            'difficult': 'Сложность'
        }


class EmployeeForm(ModelForm):
    class Meta:
        model = Employee
        fields = ['fio', 'role', 'classification', 'phone']
        labels = {'classification': 'Класс'}

    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Факс должен иметь формат: '+999999999'. До 15 цифр и символом + в начале "
                                         "строки")

    phone = forms.CharField(max_length=16, validators=[phone_regex], label='Телефон')


class UserEmployeeForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'password', 'email']

    username = forms.CharField(max_length=255, label='Пользователь')
    password = forms.CharField(max_length=255, label='Пароль')
    email = forms.EmailField(max_length=255)


class EmployeeCharacteristicsForm(ModelForm):
    class Meta:
        model = EmployeeCharacteristic
        exclude = ['date_start', 'date_finish']
        labels = {'employee':'Сотрудник', 'date_start': 'Дата начала отпуска', 'date_finish': 'Дата конца отпуска'}

    size = 30
    date_start = forms.DateField(widget=DateWidget(usel10n=True, bootstrap_version=3), label='Дата начала отпуска')
    date_finish = forms.DateField(widget=DateWidget(usel10n=True, bootstrap_version=3), label='Дата конца отпуска')
    qualification = forms.ChoiceField(choices=[(str(i), i) for i in range(0, size+1)], label='Квалификация')

