import datetime
from base64 import decode

from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.core.exceptions import ValidationError
from django.http import JsonResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from administrator.tools import getJudgeLoad

from administrator.forms import ClassificationForm, EmployeeForm, UserEmployeeForm, EmployeeCharacteristicsForm
from administrator.models import EmployeeCharacteristic
from user.models import Employee, Role, Setting, Classification
from user.tools import checkEmployee
from manager.tools import getPath, getChildClassifications
from administrator.tools import isDeletableClassifiaction, getDeleteClassifications


@login_required(login_url='login_page')
def employees(request):
    if checkEmployee(request.user, Role.ROLE_ADMIN):
        if request.method == 'GET':
            empl = Employee.objects.all()
            return render(request, 'employees.html', {'employees': empl})
    else:
        return JsonResponse({'error': 'permission denied'})


@login_required(login_url='login_page')
def employee_update(request, employee_id):
    if checkEmployee(request.user, Role.ROLE_ADMIN):
        if request.method == 'GET':
            employee = Employee.objects.get(id=employee_id)
            form_employee = EmployeeForm(instance=employee)
            employee.user.password = ""
            form_user_employee = UserEmployeeForm(instance=employee.user)
            return render(request, 'employee_update_form.html',
                          {'form_employee': form_employee, 'form_user_employee': form_user_employee,
                           'employee_id': employee_id})
        elif request.method == 'POST':
            employee = Employee.objects.get(id=employee_id)
            user = employee.user
            fio = request.POST['fio']
            username = request.POST['username']
            password = request.POST['password']
            phone = request.POST['phone']
            email = request.POST['email']
            role_id = request.POST['role']
            classification = request.POST.getlist('classification', [])
            employee.fio = fio
            role = Role.objects.get(id=role_id)
            employee.role = role
            employee.classification = classification
            employee.phone = phone
            user.username = username
            user.save(update_fields=['password'])
            user.email = email
            form_user_employee = UserEmployeeForm(request.POST, instance=user)
            form_employee = EmployeeForm(request.POST, instance=employee)
            if form_employee.is_valid() and form_user_employee.is_valid():
                form_employee.save()
                form_user = form_user_employee.save(commit=False)
                form_user.password = make_password(password)
                form_user.save()
                form_user_employee.save()
                return JsonResponse({'status': 'success'})
            else:
                return JsonResponse({'status': 'error',
                                     'message_employee': str(form_employee.errors.as_ul()),
                                     'message_user_employee': str(form_user_employee.errors.as_ul())})


@login_required(login_url='login_page')
def employees_characteristics(request):
    if checkEmployee(request.user, Role.ROLE_ADMIN) or checkEmployee(request.user, Role.ROLE_MANAGER):
        if request.method == 'GET':
            employees_characteristics = EmployeeCharacteristic.objects.all()
            result = getJudgeLoad(employees_characteristics)
            return render(request, 'employee_characteristics_admin.html', {'employees': result})
    else:
        return JsonResponse({'error': 'permission denied'})


@login_required(login_url='login_page')
def settings(request):
    if checkEmployee(request.user, Role.ROLE_ADMIN):
        if request.method == 'GET':
            enable_value = Setting.getValueById(Setting.ENABLE_AUTO_CLASSIFICATION)
            if (enable_value == '1'):
                enable = True
            else:
                enable = False
            daily_count = Setting.getValueById(Setting.JUDGE_DAILY_CLAIM_COUNT)
            return render(request, 'setting.html',
                          {
                              'enable_auto': enable,
                              'setting_enable_id': Setting.ENABLE_AUTO_CLASSIFICATION,
                              'daily_count': daily_count,
                              'daily_count_id': Setting.JUDGE_DAILY_CLAIM_COUNT
                          })
    else:
        return JsonResponse({'error': 'permission denied'})


def settings_update(request, setting_id):
    if checkEmployee(request.user, Role.ROLE_ADMIN):
        setting = Setting.objects.filter(id=setting_id).get()
        setting.value = request.POST['value']
        setting.save()
        return JsonResponse({'status': 'success'})
    else:
        return JsonResponse({'status': 'error'})


@csrf_exempt
@login_required(login_url='login_page')
def classification_list(request):
    if checkEmployee(request.user, Role.ROLE_ADMIN):
        if request.method == 'GET':
            form = ClassificationForm()
            classifications = getChildClassifications()
            result = []
            for classification in list(classifications):
                result.append({
                    'item': classification,
                    'path': getPath(classification),
                })
            return render(request, 'classifications.html', {
                'classifications': result,
                'form': form,
                'remove_classifications': getDeleteClassifications()
            })
        elif request.method == 'POST':
            classification = Classification()
            form = ClassificationForm(request.POST, instance=classification)
            if form.is_valid():
                form.save()
                return JsonResponse({'status': 'success'})
            else:
                return JsonResponse({'status': 'error', 'message': str(form.errors.as_ul())})
    else:
        return JsonResponse({'error': 'permission denied'})


@csrf_exempt
@login_required(login_url='login_page')
def classification_delete(request):
    if checkEmployee(request.user, Role.ROLE_ADMIN):
        if request.method == "POST":
            classification_ids = request.POST.getlist("delete_classification_ids", [])
            if len(classification_ids) == 0:
                return JsonResponse({'status': 'error', 'message': 'Выберите классы'})
            classifications = Classification.objects.filter(id__in=classification_ids)
            result_error = ''
            for classification in classifications:
                if isDeletableClassifiaction(classification):
                    classification.delete()
                else:
                    result_error = result_error + "</br>" + classification.name + " невозможно удалить"
            if result_error == '':
                return JsonResponse({'status': 'success'})
            else:
                return JsonResponse({'status': 'error', 'message': result_error})

    else:
        return JsonResponse({'error': 'permission denied'})


def employee_characteristics_create(request):
    if checkEmployee(request.user, Role.ROLE_ADMIN):
        if request.method == "POST":
            employee = request.POST['employee']
            qualification = request.POST['qualification']
            employee_characteristics = EmployeeCharacteristic()
            employee = Employee.objects.get(id=employee)
            employee_characteristics.date_finish = datetime.datetime.strptime(request.POST['date_finish'],
                                                                              '%d.%m.%Y').strftime('%Y-%m-%d')
            employee_characteristics.date_start = datetime.datetime.strptime(request.POST['date_start'],
                                                                             '%d.%m.%Y').strftime('%Y-%m-%d')
            employee_characteristics.employee = employee
            employee_characteristics.qualification = qualification
            employee_form = EmployeeCharacteristicsForm(request.POST, instance=employee_characteristics)
            if employee_characteristics.date_finish < employee_characteristics.date_start:
                employee_form.add_error(
                    'date_start',
                    'Дата конца отпуска не может быть раньше начала'
                )

            if employee_form.is_valid():
                employee_form.save()
                return JsonResponse({'status': 'success'})
            else:
                return JsonResponse({'status': 'error', 'message': str(employee_form.errors.as_ul())})
        elif request.method == "GET":
            employee_form = EmployeeCharacteristicsForm()
            return render(request, 'employee_characteristics_create.html', {'employee_form': employee_form})
    else:
        return JsonResponse({'error': 'permission denied'})


def employee_delete(request, employee_id):
    if checkEmployee(request.user, Role.ROLE_ADMIN):
        employee = Employee.objects.get(id=employee_id)
        employee.delete()
        return redirect(to='employees')
    else:
        return JsonResponse({'error': 'permission denied'})