from django.db import models

# Create your models here.
from robo import settings
from user.models import Employee


class EmployeeCharacteristic(models.Model):

    employee = models.ForeignKey(Employee)
    date_start = models.DateField()
    date_finish = models.DateField()
    qualification = models.PositiveSmallIntegerField(default=1)