from datetime import datetime

from django.db.models import Count, Q, QuerySet

from user.models import Classification, Employee, Claim, Setting, StatusClaim


def getUseClassifications():
    employee_use_classifications = list(Employee.objects.values_list('classification', flat=True).exclude(
        classification__isnull=True).distinct())
    claim_use_classifications = list(Claim.objects.values_list('classification_id', flat=True).exclude(
        classification_id__isnull=True).distinct())
    return employee_use_classifications + list(set(claim_use_classifications) - set(employee_use_classifications))


def isDeletableClassifiaction(classification_id):
    if (classification_id in getUseClassifications()):
        return False
    else:
        return True


def getDeleteClassifications():
    return Classification.objects.exclude(id__in=getUseClassifications()).filter(enable=True).all()


def getJudgeLoad(employees_characteristics):
    employee_ids = (o.employee_id for o in employees_characteristics)
    max_daily = int(Setting.getValueById(Setting.JUDGE_DAILY_CLAIM_COUNT))
    # @Todo DO WTIH ONE QUERY
    # all_claim_count = Claim.objects.filter(~Q(assign_date=datetime.now().date())).filter(judge_id__in=employee_ids).filter(
    #     Q(status_id__in=StatusClaim.getClaimNotFinishedStatuses()) | Q(status_id__isnull=True)).all().values('judge_id').annotate(sum=Count('id'))

    claim_count = Claim.objects.filter(assign_date=datetime.now().date()).filter(judge_id__in=employee_ids).filter(
        Q(status_id__in=StatusClaim.getClaimNotFinishedStatuses()) | Q(status_id__isnull=True)).all().values(
        'judge_id').annotate(total=Count('id'))
    claim_count_by_judge_id = {}
    for data in claim_count:
        claim_count_by_judge_id[data['judge_id']] = data['total']
    # all_claim_count_by_judge_id = {}
    # for data in all_claim_count:
    #     all_claim_count_by_judge_id[data['judge_id']] = data['sum']
    result = []
    for employee in employees_characteristics:
        employee_id = employee.employee_id
        percent = claim_count_by_judge_id.get(employee_id, 0)
        if (percent != 0):
            percent = round(percent * 100 / max_daily)
        # all_count = all_claim_count_by_judge_id.get(employee_id)
        all_count = Claim.objects \
            .filter(~Q(assign_date=datetime.now().date())) \
            .filter(judge_id=employee_id) \
            .filter(
            Q(status_id__in=StatusClaim.getClaimNotFinishedStatuses()) | Q(status_id__isnull=True)) \
            .count()
        if (all_count != 0):
            # all_count = all_claim_count_by_judge_id[employee_id]
            string = str(percent) + "% (" + str(all_count) + " не завершенных дел) "
        else:
            string = str(percent) + "%"
        result.append({'item': employee, 'load': string})
    return result
