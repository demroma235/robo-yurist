from django.conf.urls import url, include
from django.contrib import admin

from administrator import views

urlpatterns = [
    url(r'^employees/$', views.employees, name='employees'),
    url(r'^employees_characteristics/$', views.employees_characteristics, name='employees_characteristics'),
    url(r'^employee_characteristics_create/$', views.employee_characteristics_create, name='employee_characteristics_create'),
    url(r'^employee/update/(?P<employee_id>\d+)$', views.employee_update, name='employee_update'),
    url(r'^settings/$', views.settings, name='settings'),
    url(r'^classification-list/$', views.classification_list, name='classification_list'),
    url(r'^classification-delete/$',views.classification_delete, name='classification_delete'),
    url(r'^settings/update/(?P<setting_id>\d+)$', views.settings_update, name='settings_update'),
    url(r'^employee/delete/(?P<employee_id>\d+)$', views.employee_delete, name='employee_delete'),
]