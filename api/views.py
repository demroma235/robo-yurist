from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from user.models import Employee, Role, Claim, Classification, Setting


def getData(request):
    setting_auto = Setting.objects.get(id=Setting.ENABLE_AUTO_CLASSIFICATION)
    print(setting_auto.value)
    if setting_auto.value == str(1):
        role_judge = Role.objects.get(id=Role.ROLE_JUDGE)
        judges = Employee.objects.filter(role=role_judge).values()
        claims = Claim.objects.filter(judge=None).values()
        classifications = Classification.objects.all().values()
        load = Setting.objects.get(id=Setting.JUDGE_DAILY_CLAIM_COUNT)
        return JsonResponse({'status': 'success', 'data': {'judges': list(judges), 'claims': list(claims),
                                                           'classifications': list(classifications),
                                                           'load': str(load.value)}})
    else:
        return JsonResponse({'status': 'error', 'text': 'autoappointment doesn\'t work'})


def setClaimToJudge(request, judge_id, claim_id):
    try:
        claim = Claim.objects.get(id=claim_id)
        judge = Employee.objects.get(id=judge_id)
        claim.judge = judge
        claim.save()
        return JsonResponse({'status': 'success'})
    except:
        return JsonResponse({'status': 'error', 'text': 'some error'})