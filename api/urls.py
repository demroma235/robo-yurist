from django.conf.urls import url, include
from django.contrib import admin

from api import views

urlpatterns = [
    url(r'^data$', views.getData, name='data_api'),
    url(r'^(?P<judge_id>\d+)/(?P<claim_id>\d+)$', views.setClaimToJudge, name='set_claim_to_judge')
]