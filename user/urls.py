from django.conf.urls import url, include
from django.contrib import admin

from user import views

urlpatterns = [
    url(r'^$', views.default, name='default'),
    url(r'^claims$', views.list_claims, name='list_claims'),
    url(r'^login$', views.login_page, name='login_page'),
    url(r'^logout$', views.make_logout, name='logout'),
    url(r'^claim/add$', views.add_claim, name='add_claim'),
    # url(r'^claim/(?P<claim_id>\d+)$', views.claim_page, name='claim_page'),
    url(r'^claim/upload-file$', views.add_claim_file, name='add_claim_file')


]