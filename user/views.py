import datetime
import os

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.sessions import serializers
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.db.models import Q
from docx import Document

from judge.tools import getFactsClaimsId, getClaimCaseHref
from user.tools import generateDocByClaim, checkEmployee
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
from user.forms import LoginForm, ClaimForm, SecondStatusClaimHistoryForm
from user.models import Claim, Profile, File, StatusClaim, StatusClaimHistory, Role, Employee


@login_required(login_url='login_page')
def default(request):
    return render(request, 'base.html')


@login_required(login_url='login_page')
def list_claims(request):
    if request.method == 'GET':
        try:
            u = Profile.objects.get(user=request.user)
        except ObjectDoesNotExist:
            return redirect(to='employees')
        claims = Claim.objects.filter(user=u).filter(~Q(status_id=StatusClaim.STATUS_ARCHIVE))
        return render(request, 'user_claims.html', {'claims': claims})


def login_page(request, error=None):
    if request.user.is_authenticated:
        try:
            us = Profile.objects.get(user_id=request.user)
        except ObjectDoesNotExist:
            return redirect(to='employees')
    if request.method == 'GET':
        if request.user.is_authenticated:
            return redirect(to='list_claims')
        else:
            lf = LoginForm()
            return render(request, 'login.html', {'login_form': lf,
                                                  'error': error})
    elif request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect(to='/')
        else:
            data = request.POST.copy()

            lf = LoginForm(initial=data)
            return render(request, 'login.html', {'login_form': lf,
                                                  'error': 'Ошибка авторизации'})


def make_logout(request):
    logout(request)
    return redirect(to='login_page')


@login_required(login_url='login_page')
def add_claim(request):
    if request.user.is_authenticated:
        try:
            us = Profile.objects.get(user_id=request.user)
        except ObjectDoesNotExist:
            return redirect(to='employees')
    if request.method == "POST":
        files = request.FILES.getlist('files')
        claim = Claim()
        claim.user = Profile.objects.get(user_id=request.user)
        claim.fax = request.POST['fax']
        claim.fax_representative = request.POST['fax_representative']
        claim.fax_defendant = request.POST['fax_defendant']

        cf = ClaimForm(request.POST, instance=claim)
        if cf.is_valid():
            cf.save()
            for file in files:
                f = File()
                f.file = file
                f.name = 'test'
                f.save()
                claim.files.add(f)
            claim.save()
            generateDocByClaim(claim)
            cf = ClaimForm(request.POST, instance=claim)
            s = cf.save(commit=False)
            s.date_of_birthday = datetime.datetime.strptime(request.POST['date_of_birthday'],
                                                            '%d.%m.%Y').strftime('%Y-%m-%d')
            s.date_of_birthday_defendant = datetime.datetime.strptime(request.POST['date_of_birthday_defendant'],
                                                                      '%d.%m.%Y').strftime('%Y-%m-%d')
            s.date_of_birthday_representative = datetime.datetime.strptime(
                request.POST['date_of_birthday_representative'],
                '%d.%m.%Y').strftime('%Y-%m-%d')
            s.date = datetime.datetime.strptime(
                request.POST['date'],
                '%d.%m.%Y').strftime('%Y-%m-%d')
            s.save()
            return redirect(to='list_claims')
        else:

            return render(request, 'add_claim.html', {'form': cf})

    elif request.method == "GET":
        form = ClaimForm()
        return render(request, 'add_claim.html', {'form': form})


@csrf_exempt
@login_required(login_url='login_page')
def add_claim_file(request):
    if request.user.is_authenticated and request.method == "POST":
        file = request.FILES.get('file')
        filename = request.POST.get('filename')
        f = File()
        f.file = file
        f.name = filename
        f.save()
        return JsonResponse({id: f.id})
    else:
        raise Exception()


# @login_required(login_url='login_page')
# def claim_page(request, claim_id):
#         if request.method == 'GET':
#             empl = Employee.objects.get(user_id=request.user)
#             claim = Claim.objects.get(judge=empl, id=claim_id)
#             claim_file_path = str(claim.claim_file.file)
#             filename, file_extension = os.path.splitext(claim_file_path)
#             filepath = os.path.join(settings.MEDIA_ROOT, claim_file_path)
#             f = open(filepath, 'rb')
#             content = ''
#             if (claim.claim_file.name not in getFactsClaimsId()):
#                 is_mark = False
#                 if (file_extension == '.docx'):
#                     doc = Document(f)
#                     fullText = []
#                     for para in doc.paragraphs:
#                         fullText.append(para.text)
#                     content = '\n'.join(fullText)
#             else:
#                 is_mark = True
#                 content = getClaimCaseHref(claim.claim_file.name)
#             claim_history = StatusClaimHistory()
#             claim_history.claim_id = claim.id
#             form = SecondStatusClaimHistoryForm(instance=claim_history)
#             form.old_status = claim.status
#             form.new_status = claim.status
#             return render(request, 'claim_page.html', {
#                 'is_mark': is_mark,
#                 'content': content,
#                 'form': form,
#                 'claim': claim
#             })
