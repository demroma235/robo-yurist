import datetime
from datetime import datetime as date_now

import django
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db import models


# Create your models here.
class File(models.Model):
    name = models.CharField(max_length=128)
    file = models.FileField(upload_to='docs/')

class ClaimFile(models.Model):
    name = models.CharField(max_length=128)
    file = models.FileField(upload_to='claims/')

class Classification(models.Model):
    def __str__(self):
        return self.name

    parent = models.ForeignKey('self', blank=True, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=256, unique=True)
    enable = models.BooleanField(default=True)
    difficult = models.PositiveSmallIntegerField(default=1)

class Role(models.Model):
    ROLE_MANAGER = 1
    ROLE_JUDGE = 2
    ROLE_ADMIN = 3

    def __str__(self):
        return self.name

    name = models.CharField(max_length=256, unique=True)
    sys_name = models.CharField(max_length=256, unique=True)


class Profile(models.Model):
    def __str__(self):
        return self.user.username

    user = models.OneToOneField(User)


class Employee(models.Model):
    def __str__(self):
        return ' / '.join([self.fio, self.role.name])

    class Meta:
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'

    user = models.OneToOneField(User, verbose_name="Пользователь")
    fio = models.CharField(max_length=256, verbose_name='ФИО')
    phone = models.CharField(max_length=256, verbose_name='Телефон')
    role = models.ForeignKey(Role, on_delete=models.SET_NULL, null=True, verbose_name='Роль')
    classification = models.ManyToManyField(Classification, blank=True, null=True, verbose_name='Классификация')


class StatusClaim(models.Model):
    def __str__(self):
        return self.name

    STATUS_ACCEPT = 1
    STATUS_RETURN = 2
    STATUS_ARCHIVE = 3
    STATUS_STOP = 4
    STATUS_REJECT = 5
    STATUS_GRANT = 6
    STATUS_GRANT_PARTIALLY = 7
    STATUS_PROCESSING = 8

    @staticmethod
    def getClaimNotFinishedStatuses():
        return [StatusClaim.STATUS_STOP, StatusClaim.STATUS_ACCEPT]

    name = models.TextField(max_length=256)


class Claim(models.Model):
    def __str__(self):
        return self.name

    user = models.ForeignKey(Profile, null=True)
    judge = models.ForeignKey(Employee, blank=True, null=True)
    status = models.ForeignKey(StatusClaim, blank=True, null=True, default=StatusClaim.STATUS_PROCESSING, editable=False)

    __previous_judge_id = None

    def __init__(self, *args, **kwargs):
        super(Claim, self).__init__(*args, **kwargs)
        self.__previous_judge_id = self.judge_id

    # Сохранение даты назначения на судью
    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        if self.judge_id != None and self.judge_id != self.__previous_judge_id:
            self.assign_date = date_now.now().date()

        super(Claim, self).save(force_insert, force_update, *args, **kwargs)
        self.__previous_judge_id = self.judge_id

    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,11}$',
                                 message="Факс должен иметь формат: '+999999999'. До 11 цифр и символ +")

    name = models.CharField(max_length=256,verbose_name='Наименование заявления')
    court = models.CharField(max_length=256,verbose_name='Арбитражный суд')
    plaintfill = models.CharField(max_length=256,verbose_name='Представитель')
    address = models.CharField(max_length=256,verbose_name='Адрес')
    fax = models.CharField(max_length=256,verbose_name='Телефон',validators=[phone_regex])
    email = models.EmailField(max_length=256,verbose_name='Адрес электронной почты')
    date_of_birthday = models.DateField(verbose_name='Дата рождения')

    plaintfill_representative = models.CharField(max_length=256,verbose_name='Представитель')
    address_representative = models.CharField(max_length=256,verbose_name='Адрес')
    fax_representative = models.CharField(max_length=256,verbose_name='Телефон',validators=[phone_regex])
    email_representative = models.EmailField(max_length=256,verbose_name='Адрес электронной почты')
    date_of_birthday_representative = models.DateField(verbose_name='Дата рождения')

    defendant = models.CharField(max_length=256,verbose_name='Представитель')
    address_defendant = models.CharField(max_length=256,verbose_name='Адрес')
    fax_defendant = models.CharField(max_length=256,verbose_name='Телефон',validators=[phone_regex])
    email_defendant = models.EmailField(max_length=256,verbose_name='Адрес электронной почты')
    date_of_birthday_defendant = models.DateField(verbose_name='Дата рождения')

    assign_date = models.DateField(blank=True, null=True)

    date = models.DateField(default=django.utils.timezone.now,verbose_name='Дата')
    files = models.ManyToManyField(File,verbose_name='Приложение')
    claim_file = models.ForeignKey(ClaimFile, blank=True, null=True)

    claim_body = models.TextField(verbose_name='Исковое заявление')
    claim_request  = models.TextField(verbose_name='Прошу')

    classification = models.ForeignKey(Classification, blank=True, null=True)


class StatusClaimHistory(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    reason = models.TextField(verbose_name='На основании чего')
    new_status = models.ForeignKey(StatusClaim, null=True, related_name='new_status',verbose_name='Новый статус')
    old_status = models.ForeignKey(StatusClaim, null=True, related_name='old_status',verbose_name='Текущий статус')

    claim = models.ForeignKey(Claim)


class Setting(models.Model):
    ENABLE_AUTO_CLASSIFICATION = 1
    JUDGE_DAILY_CLAIM_COUNT = 2

    value = models.CharField(max_length=256)

    def getValueById(id):
        return Setting.objects.filter(id=id).get().value
