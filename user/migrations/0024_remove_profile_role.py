# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-11 14:12
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0023_auto_20170511_1650'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='role',
        ),
    ]
