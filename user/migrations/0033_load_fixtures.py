from __future__ import unicode_literals

from datetime import datetime

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import os,shutil

def create_claim(claim,claim_file,judge,user):
    claim.claim_file = claim_file
    claim.user = user
    claim.judge = judge
    claim.name = 'тест'
    claim.court = 'тест'
    claim.plaintfill = 'Заявление '+claim_file.name
    claim.address = 'тест'
    claim.fax = '12345'
    claim.email = 'test@mail.com'
    claim.date_of_birthday = '1995-01-01'

    claim.plaintfill_representative = 'тест'
    claim.address_representative = 'тест'
    claim.fax_representative = '12345'
    claim.email_representative = 'test@mail.com'
    claim.date_of_birthday_representative = '1995-01-01'

    claim.defendant = 'тест'
    claim.address_defendant = 'тест'
    claim.fax_defendant = '123456'
    claim.email_defendant = 'test@mail.com'
    claim.date_of_birthday_defendant = '1995-01-01'
    claim.date = datetime.now()
    claim.save()

def load_path(apps, schema_editor):
    media_dir = os.path.join(*[os.getcwd(),'media','claims'])
    if not os.path.isdir(media_dir):
        os.makedirs(media_dir)
    ClaimFile = apps.get_model('user', 'ClaimFile')
    Claim = apps.get_model('user','Claim')
    Employee = apps.get_model('user','Employee')
    Profile = apps.get_model('user','Profile')
    Role = apps.get_model('user','Role')
    judge_role = Role.objects.get(sys_name='ROLE_JUDGE')
    judge_list = list(Employee.objects.filter(role=judge_role).all())
    if not judge_list:
        judge = None
    else:
        judge =judge_list[0]
    user = list(Profile.objects.all())
    if not user:
        user = None
    else:
        user = user[0]
    for file in os.listdir("test_claims"):
        if file.endswith(".txt"):
            path1 = os.path.join(os.getcwd(),os.path.join("test_claims", file))
            path2 = os.path.join(media_dir,file)
            filename = os.path.splitext(file)[0]
            claim_file = ClaimFile()
            claim_file.name = filename
            claim_file.file = os.path.join('claims',file)
            claim_file.save()
            claim = Claim()
            create_claim(claim,claim_file,judge,user)
            shutil.copy2(path1, path2)

class Migration(migrations.Migration):

    dependencies = [
        ('user', '0032_add_test_claims'),
    ]

    operations = [
        migrations.RunPython(load_path)
    ]