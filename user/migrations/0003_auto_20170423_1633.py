# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-04-23 13:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0002_auto_20170423_1602'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='classification',
            field=models.ManyToManyField(null=True, to='user.Classification'),
        ),
    ]
