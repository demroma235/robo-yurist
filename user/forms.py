from datetimewidget.widgets import DateWidget
from django import forms
from django.core.validators import RegexValidator
from django.forms import ModelForm
from user.models import Claim, StatusClaimHistory, StatusClaim
from user.widgets import MultiFileInput
from django.db.models import Q
from django.core import validators


class LoginForm(forms.Form):
    username = forms.CharField(label='Логин', max_length=64)
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput)

class StatusClaimHistoryForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(StatusClaimHistoryForm, self).__init__(*args, **kwargs)
        ids = [StatusClaim.STATUS_GRANT_PARTIALLY, StatusClaim.STATUS_GRANT, StatusClaim.STATUS_REJECT]
        self.fields['new_status'].queryset = StatusClaim.objects.filter(~Q(id=self.instance.claim.status_id))

    class Meta:
        model = StatusClaimHistory
        fields = '__all__'
        exclude = ['date', 'old_status', 'claim']

class FirstStatusClaimHistoryForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FirstStatusClaimHistoryForm, self).__init__(*args, **kwargs)
        ids = [StatusClaim.STATUS_GRANT_PARTIALLY, StatusClaim.STATUS_GRANT, StatusClaim.STATUS_REJECT]
        self.fields['new_status'].queryset = StatusClaim.objects.filter(~Q(id=self.instance.claim.status_id)).exclude(id__in=ids)

    class Meta:
        model = StatusClaimHistory
        fields = '__all__'
        exclude = ['date', 'old_status', 'claim']


class SecondStatusClaimHistoryForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SecondStatusClaimHistoryForm, self).__init__(*args, **kwargs)
        ids = [StatusClaim.STATUS_GRANT_PARTIALLY, StatusClaim.STATUS_GRANT, StatusClaim.STATUS_REJECT]
        self.fields['new_status'].queryset = StatusClaim.objects.filter(~Q(id=self.instance.claim.status_id)).filter(id__in=ids)

    class Meta:
        model = StatusClaimHistory
        fields = '__all__'
        exclude = ['date', 'old_status', 'claim']


class ClaimForm(ModelForm):
    class Meta:
        model = Claim
        fields = '__all__'
        exclude = ['judge', 'user', 'status', 'classification', 'files', 'assign_date', 'claim_file']
        widgets = {
            'date_of_birthday': DateWidget(usel10n=True, bootstrap_version=3),
            'date_of_birthday_defendant': DateWidget(usel10n=True, bootstrap_version=3),
            'date_of_birthday_representative': DateWidget(usel10n=True, bootstrap_version=3),
            'date': DateWidget(usel10n=True, bootstrap_version=3)
        }

    list_item = [
        ('Арбитражный суд', 'Арбитражный суд')
    ]
    court = forms.ChoiceField(choices=list_item)
