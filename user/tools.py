import os

from docxtpl import DocxTemplate
from django.conf import settings

from user.models import Role, Employee, ClaimFile


def checkEmployee(user, role_id):
    r = Role.objects.get(id=role_id)
    try:
        Employee.objects.get(user_id=user.id, role=r)
        return True
    except:
        return False


def generateDocByClaim(claim):
    doc_name = claim.name + str(claim.id)
    doc_path = 'claims/' + doc_name + '.docx'
    doc = DocxTemplate(settings.BASE_DIR + "/claim.docx")
    index = 0
    applys = []
    for file in claim.files.all():
        applys.append({'index': index, "name": file.name})
        index += 1
    doc.render({"claim": claim, 'applys': applys})
    doc.save(os.path.join(settings.MEDIA_ROOT , doc_path))
    claim_file = ClaimFile()
    claim_file.file = doc_path
    claim_file.name = doc_name
    claim_file.save()
    claim.claim_file_id = claim_file.id
    claim.save()
