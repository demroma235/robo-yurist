from django.contrib import admin

# Register your models here.
from user.models import Profile, Role, Classification, Setting, \
    Employee, Claim, StatusClaim,File,StatusClaimHistory,ClaimFile

admin.site.register(Profile)
admin.site.register(Role)
admin.site.register(Classification)
admin.site.register(Setting)
admin.site.register(Employee)
admin.site.register(Claim)
admin.site.register(StatusClaim)
admin.site.register(File)
admin.site.register(StatusClaimHistory)
admin.site.register(ClaimFile)


